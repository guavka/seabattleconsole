# Игра морской бой 2 + 3 + 2 + 2 + 3 часа
import math
import os
import random

alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
            "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

user1_field = []
user2_field = []

user1_visual = []
user2_visual = []

user1_ships = []
user2_ships = []

column_count = 10
row_count = 10

# Ships count
lincor_count = 1
cruiser_count = 2
destroyer_count = 3
boat_count = 4


def ClearConsole():
    command = 'clear'
    if os.name in ('nt', 'dos'):  # If Machine is running on Windows, use cls
        command = 'cls'
    os.system(command)


def PrintField(message, field):
    def PrintHorizontalLine(count):
        print(" ", end='')
        for i in range(count * 3 + 1):
            print("-", end='')
        print()
    # print Header
    tab_count = math.floor((column_count * 3 - len(message)) / 2)
    print(f"{' '*tab_count}{message}")
    print("  ", end='')
    for letter in alphabet[:column_count:]:
        print(f"|{letter}|", end='')
    print()
    PrintHorizontalLine(column_count)
    loc_index = 1
    for row in field:
        if loc_index // 10 == 0:
            print(" ", end='')
        print(f"{loc_index}", end='')
        for column in row:
            print(f"|{column}|", end='')
        print()
        loc_index += 1
    PrintHorizontalLine(column_count)


def InputIndexes():
    while True:
        user_column = input("Select column: ")
        if user_column in alphabet[:column_count:]:
            index = alphabet.index(user_column)
            break
        else:
            print("Bad column")
    while True:
        user_row = input("Select row: ")
        try:
            user_row = int(user_row)
        except:
            print("Bad row!")
        else:
            user_row -= 1
            if user_row < row_count and user_row >= 0:
                break
            print(f"Row must be in range [1..{row_count}]")
    return [user_row, index]


def GetDirection(direction=None):
    while True:
        if direction == None:
            direction = input(
                "Select direction:\n1.Top\n2.Down\n3.Left\n4.Right\n")
        if direction == "1":
            return "TOP"
        elif direction == "2":
            return "DOWN"
        elif direction == "3":
            return "LEFT"
        elif direction == "4":
            return "RIGHT"
        else:
            print("Bad menu item")


def CheckPlace(field, indexes, size=1, vector="RIGHT"):
    def CheckCells(field, row, col):
        def CheckEmpty(field, low, high, left, right):
            flag = True
            for rCell in range(low, high + 1):
                for cCell in range(left, right + 1):
                    try:
                        if rCell < 0 or cCell < 0:
                            return False
                        if field[rCell][cCell] != "~":
                            return False
                    except:
                        return False
            return flag

        low_index = row - 1
        high_index = row + 1

        left_index = col - 1
        right_index = col + 1

        if row - 1 < 0:
            low_index = row
        elif row + 1 == row_count:
            high_index = row

        if col - 1 >= 0:
            left_index = col - 1
            if col + 1 < column_count:
                right_index = col + 1
            else:
                right_index = col
        else:
            left_index = col
            if col + 1 < column_count:
                right_index = col + 1
            else:
                right_index = col

        return CheckEmpty(field, low_index, high_index, left_index, right_index)

    row = indexes[0]
    col = indexes[1]

    size -= 1

    ship_data = []

    r_range = [row]
    c_range = [col]

    if vector == "TOP":
        r_range = range(row - size, row + 1)
    elif vector == "DOWN":
        r_range = range(row, row + size + 1)
    elif vector == "LEFT":
        c_range = range(col - size, col + 1)
    elif vector == "RIGHT":
        c_range = range(col, col + size + 1)

    for r_cell in r_range:
        for c_cell in c_range:
            if CheckCells(field, r_cell, c_cell):
                ship_data.append([r_cell, c_cell])
            else:
                return None
    return ship_data


def PlaceBoatsMenu(message, user_list, user_ships, bot=False):
    count = 0
    summ = lincor_count + cruiser_count + destroyer_count + boat_count

    def PlaceBoats(ship_data, index):
        for ship in ship_data:
            user_list[ship[0]][ship[1]] = "O"
        user_ships[index].append([ship_data])

    def PlaceBoat(current_count, max_count, size, index):
        nonlocal count
        if current_count < max_count:
            tmp = InputIndexes()
            if size != 1:
                direction = GetDirection()
                data = CheckPlace(user_list, tmp, size, direction)
            else:
                data = CheckPlace(user_list, tmp)
            if data != None:
                PlaceBoats(data, index)
                PrintField(message, user_list)
                count += 1
            else:
                print("You cant place at this place")

    def PlaceBoatsRandom(max_count, size, index):
        import random
        nonlocal count
        current_count = 0
        while current_count < max_count:
            tmp = [random.randint(0, row_count),
                   random.randint(0, column_count)]
            direction = GetDirection(str(random.randint(1, 4)))
            data = CheckPlace(user_list, tmp, size, direction)
            if data != None:
                PlaceBoats(data, index)
                count += 1
                current_count += 1
            else:
                print("You cant place at this place")

    if bot:
        PlaceBoatsRandom(lincor_count - len(user_ships[0]), 4, 0)
        PlaceBoatsRandom(cruiser_count - len(user_ships[1]), 3, 1)
        PlaceBoatsRandom(destroyer_count - len(user_ships[2]), 2, 2)
        PlaceBoatsRandom(boat_count - len(user_ships[3]), 1, 3)

    while count != summ:
        ClearConsole()
        PrintField(message, user_list)
        print(f"You must place:\n \
1.Lincors: {lincor_count - len(user_ships[0])}\n \
2.Cruisers: {cruiser_count - len(user_ships[1])}\n \
3.Destroyers: {destroyer_count - len(user_ships[2])}\n \
4.Boats: {boat_count - len(user_ships[3])}\n \
5.Random")

        user_choose = input("Select boat type: ")
        if user_choose == "1":
            PlaceBoat(len(user_ships[0]), lincor_count, 4, 0)
        elif user_choose == "2":
            PlaceBoat(len(user_ships[1]), cruiser_count, 3, 1)
        elif user_choose == "3":
            PlaceBoat(len(user_ships[2]), destroyer_count, 2, 2)
        elif user_choose == "4":
            PlaceBoat(len(user_ships[3]), boat_count, 1, 3)
        elif user_choose == "5":
            PlaceBoatsRandom(lincor_count - len(user_ships[0]), 4, 0)
            PlaceBoatsRandom(cruiser_count - len(user_ships[1]), 3, 1)
            PlaceBoatsRandom(destroyer_count - len(user_ships[2]), 2, 2)
            PlaceBoatsRandom(boat_count - len(user_ships[3]), 1, 3)
        else:
            print("Bad menu item!")


is_play_game = True
is_with_bot = False


def Start():
    turn = True
    is_continue = True
    while is_continue:
        ClearConsole()

        def Turn(player_name, field, visual, enemy):
            def GetShip(ships_info, coords):
                def GetShipsCount():
                    count = 0
                    for ships_type in ships_info:
                        for ships in ships_type:
                            count += len(ships)
                    return count

                def ShootShip(info):
                    nonlocal is_continue
                    global is_play_game
                    ships_info[info[0]][info[1]][info[2]].remove(coords)
                    if len(ships_info[info[0]][info[1]][info[2]]) == 0:
                        ships_info[info[0]][info[1]].remove(
                            ships_info[info[0]][info[1]][info[2]])
                        if GetShipsCount() == 0:
                            print(f"{player_name} Win")
                            user_choose = input("1.Restart\n2.Exit")
                            if user_choose == "1":
                                is_continue = False
                            elif user_choose == "2":
                                is_continue = False
                                is_play_game = False
                            else:
                                print("Bad menu item")

                for ship_type in ships_info:
                    for ship in ship_type:
                        for data in ship:
                            if coords in data:
                                ship_type_index = ships_info.index(ship_type)
                                ship_index = ship_type.index(ship)
                                data_index = ship.index(data)
                                ShootShip([ship_type_index,
                                          ship_index, data_index])

            nonlocal turn
            PrintField(f"{player_name} ships field", field)
            PrintField(f"{player_name} attack field", visual)
            if (not turn) and is_with_bot:
                tmp = [random.randint(0, column_count),
                       random.randint(0, row_count)]
            else:
                tmp = InputIndexes()
            cell_val = enemy[tmp[0]][tmp[1]]
            if cell_val == "O":
                enemy[tmp[0]][tmp[1]] = "X"
                visual[tmp[0]][tmp[1]] = "X"

                if turn:
                    GetShip(user2_ships, tmp)
                else:
                    GetShip(user1_ships, tmp)

            elif cell_val == "*":
                print("Error! Input another coords")
            else:
                enemy[tmp[0]][tmp[1]] = "*"
                visual[tmp[0]][tmp[1]] = "*"
                turn = not turn

        if turn:
            Turn("User1", user1_field, user1_visual, user2_field)
        else:
            Turn("User2", user2_field, user2_visual, user1_field)


def Initialize():
    def GenerateFields():
        def GenerateList(new_list):
            for row in range(1, row_count + 1):
                row_list = []
                for column in range(1, column_count + 1):
                    row_list.append("~")
                new_list.append(row_list)

        user1_field.clear()
        user2_field.clear()

        user1_visual.clear()
        user2_visual.clear()

        GenerateList(user1_field)
        GenerateList(user2_field)
        GenerateList(user1_visual)
        GenerateList(user2_visual)

    def GenerateShipsInfo():
        global user1_ships
        global user2_ships
        user1_ships = [[], [], [], []]
        user2_ships = [[], [], [], []]

    GenerateFields()
    GenerateShipsInfo()

    global is_with_bot
    while True:
        user_choose = input("1.Play with Player\n2.Play with Bot\n")
        if user_choose == "1":
            is_with_bot = False
            break
        elif user_choose == "2":
            is_with_bot = True
            break
        else:
            print("Bad menu item")

    PlaceBoatsMenu("User1 field", user1_field, user1_ships)
    if is_with_bot:
        PlaceBoatsMenu("User2 field", user2_field, user2_ships, True)
    else:
        PlaceBoatsMenu("User2 field", user2_field, user2_ships)
    ClearConsole()
    Start()


def StartBaseGame():
    global column_count
    global row_count

    global lincor_count
    global cruiser_count
    global destroyer_count
    global boat_count

    column_count = 10
    row_count = 10

    # Ships count
    lincor_count = 1
    cruiser_count = 2
    destroyer_count = 3
    boat_count = 4

    Initialize()


def CastValue(data, type):
    try:
        data = type(data)
    except:
        print(f"Error cast to {type}")
        return None
    else:
        return data


def InputValue(msg, left, right):
    while True:
        tmp = input(msg)
        tmp = CastValue(tmp, int)
        if tmp != None:
            if tmp >= left and tmp <= right:
                return tmp
            else:
                print(f"Value must be in range [{left},{right}]")


def StartCustomGame():
    def InitBoatCount():
        global lincor_count
        global cruiser_count
        global destroyer_count
        global boat_count

        while True:
            count_row = row_count / 6
            count_column = column_count / 6
            lincor_count = InputValue(
                "Input lincor count: ", 0, count_row*count_column)
            count_row = row_count / 5
            count_column = column_count / 5
            cruiser_count = InputValue(
                "Input cruiser count: ", 0, count_row*count_column)
            count_row = row_count / 4
            count_column = column_count / 4
            destroyer_count = InputValue(
                "Input destroyer count: ", 0, count_row*count_column)
            count_row = row_count / 3
            count_column = column_count / 3
            boat_count = InputValue(
                "Input boat count: ", 0, count_row*count_column)
            if lincor_count+cruiser_count+destroyer_count+boat_count != 0:
                break
            else:
                print("You need input 1 boat")

    def InitField():
        global column_count
        global row_count

        column_count = InputValue(
            f"Input column count [6,{len(alphabet)-1}]: ", 6, len(alphabet)-1)
        row_count = InputValue(
            f"Input row count [6,{column_count*2}]: ", 6, column_count*2)

    def OnComplete():
        if lincor_count+cruiser_count+destroyer_count+boat_count == 0:
            print("You need input 1 boat")
            return

        if column_count + row_count == 0:
            print("You need input field parameters")
            return

        need_for_lincor = lincor_count * 3 * 6
        need_for_cruiser = cruiser_count * 3 * 5
        need_for_destroyer = destroyer_count * 3 * 4
        need_for_boat = boat_count * 3 * 3

        need_cells = need_for_lincor + need_for_cruiser + \
            need_for_destroyer+need_for_boat - row_count-column_count

        if need_cells > column_count * row_count:
            print("Error. Check parameters!")
        else:
            Initialize()

    # Ships count
    while True:
        ClearConsole()
        print("Select menu:")
        user_choose = input(
            "1.Input boat count\n2.Input field size\n3.Complete\n4.Back\n")
        if user_choose == "1":
            InitBoatCount()
        elif user_choose == "2":
            InitField()
        elif user_choose == "3":
            OnComplete()
        elif user_choose == "4":
            break
        else:
            print("Bad menu item")


def StartGameMenu():
    while is_play_game:
        ClearConsole()
        print("1.Base 10x10 game\n2.Custom game\n3.Back to menu")
        user_choose = input()
        if user_choose == "1":
            StartBaseGame()
        elif user_choose == "2":
            StartCustomGame()
        elif user_choose == "3":
            break
        else:
            print("Bad menu item")


def Loop():
    while True:
        ClearConsole()
        print("Welcome to Sea battle, select menu item!")
        print("1.Start game\n2.Exit")
        user_choose = input()
        if user_choose == "1":
            StartGameMenu()
        elif user_choose == "2":
            break
        else:
            print("Bad menu item")


def main():
    Loop()


main()
