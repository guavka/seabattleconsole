class InitException(Exception):
    def __init__(self, message) -> None:
        super().__init__(message)


class Direction():
    @property
    def is_top(self) -> bool:
        return self.__is_top

    @property
    def is_down(self) -> bool:
        return self.__is_down

    @property
    def is_left(self) -> bool:
        return self.__is_left

    @property
    def is_right(self) -> bool:
        return self.__is_right

    def __init__(self, direction) -> None:
        if not(direction in range(1, 4+1)):
            raise InitException("Error direction value")

        self.__is_top = direction == 1
        self.__is_down = direction == 2
        self.__is_left = direction == 3
        self.__is_right = direction == 4


class Coords():
    @property
    def get_list(self) -> list:
        return [self.row, self.column]

    @property
    def row(self) -> int:
        return self.__row

    @property
    def column(self) -> int:
        return self.__column

    def __init__(self, row, column) -> None:
        self.__row = row
        self.__column = column

    def __eq__(self, __o: object) -> bool:
        return isinstance(__o, Coords) and ((self.row, self.column) == (__o.row, __o.column))

    def __hash__(self) -> int:
        return hash((self.row, self.column))


class Ship():
    @property
    def is_live(self) -> bool:
        return self.__is_died

    @property
    def get_ship(self) -> list:
        return self.__ship_info

    @property
    def get_ship_list(self) -> list:
        tmp = []
        for coord in self.__ship_dict:
            tmp.append([coord.get_list])
        return tmp

    def check_die(self):
        for ship in self.__ship_dict.values():
            self.__is_died |= ship
        return self.__is_died

    def check_shoot(self, new_coords) -> bool:
        for coords in self.__ship_dict:
            if coords == new_coords:
                if self.__ship_dict[coords] == True:
                    self.__ship_dict[coords] = False
                return True
        return False

    def __create_ship(self) -> None:
        self.__ship_dict = {}
        base_row = self.__start_coords.row
        base_column = self.__start_coords.column
        for i in range(self.__size):
            if self.__direction.is_top:
                new_coords = Coords(base_row-i, base_column)
            elif self.__direction.is_down:
                new_coords = Coords(base_row+i, base_column)
            elif self.__direction.is_left:
                new_coords = Coords(base_row, base_column - i)
            elif self.__direction.is_right:
                new_coords = Coords(base_row, base_column + i)

            self.__ship_dict[new_coords] = True

    def __init__(self, coords, direction, size) -> None:
        if not (isinstance(coords, Coords)):
            raise InitException("Error cast coords to Coords")
        if not (isinstance(direction, Direction)):
            raise InitException("Error cast direction to Direction")
        if type(size) != int:
            raise InitException("Error cast size to int")

        self.__start_coords = coords
        self.__direction = direction
        self.__size = size

        self.__is_died = False

        self.__create_ship()


class Field():
    def __init_field():
        pass

    def __init__(self, rows, columns) -> None:
        self.__rows = rows
        self.__columns = columns

        self.__init_field()


try:
    ship = Ship(Coords(0, 0), Direction(4), 4)
except InitException as e:
    print(e)
else:
    print(ship.get_ship_list)
    print(ship.check_shoot(Coords(0, 2)))
